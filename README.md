## VAUnityExtensions

Unity extensions that require further Unity assets like SteamVR and provide advanced 3D user interfaces for interaction with VA.

### License

Copyright 2015-2022 Institute of Technical Acoustics (ITA), RWTH Aachen University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use files of this project except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

### Quick build guide

VAUnityExtensions is a collection of scripts structured by folders only. Copy the files you intend to use into your scene's asset folder and get the dependencies from the asset store.
